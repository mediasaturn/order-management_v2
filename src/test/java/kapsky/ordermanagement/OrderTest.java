package kapsky.ordermanagement;

import akka.actor.testkit.typed.javadsl.TestKitJunitResource;
import akka.actor.typed.ActorRef;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.MediaTypes;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.testkit.JUnitRouteTest;
import akka.http.javadsl.testkit.TestRoute;
import org.junit.*;

public class OrderTest extends JUnitRouteTest {
    @ClassRule
    public static TestKitJunitResource testkit = new TestKitJunitResource();

    //#test-top
    // shared registry for all tests
    private static ActorRef<Order.Command> order;
    private TestRoute appRoute;

//    @BeforeClass
//    public static void beforeClass() {
//        order = testkit.spawn(Order.create("1"));
//    }

    @Before
    public void before() {
        OrderServiceImpl orderService = new OrderServiceImpl(testkit.system());
        OrderRoutes userRoutes = new OrderRoutes(orderService);
        appRoute = testRoute(userRoutes.createRoutes());
    }

//    @AfterClass
//    public static void afterClass() {
//        testkit.stop(order);
//    }

    //#set-up
    //#actual-test
//    @Test
//    public void test1NoUsers() {
//        appRoute.run(HttpRequest.GET("/users"))
//                .assertStatusCode(StatusCodes.OK)
//                .assertMediaType("application/json")
//                .assertEntity("{\"users\":[]}");
//    }

    @Test
    public void test2HandlePOST() {
        appRoute.run(HttpRequest.POST("/createOrder")
                .withEntity(MediaTypes.APPLICATION_JSON.toContentType(),
                        "{\"itemId\": \"2\"}"))
                //.assertStatusCode(StatusCodes.CREATED)
                .assertMediaType("application/json");
                //.assertEntity("{\"description\":\"User Kapi created.\"}");
    }
}