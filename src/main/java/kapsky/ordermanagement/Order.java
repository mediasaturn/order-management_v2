package kapsky.ordermanagement;

import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Behavior;
import akka.actor.typed.SupervisorStrategy;
import akka.actor.typed.javadsl.Behaviors;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.Entity;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.*;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Order extends EventSourcedBehaviorWithEnforcedReplies<
        Order.Command, Order.Event, Order.State> {

    // States

    abstract static class State implements CborSerializable {
        public final Order order;
        protected State(Order order){
            this.order = order;
        }

    }

    public static class Open extends State {

        protected Open(Order order) {
            super(order);
        }
    }

    public static class Created extends State {

        protected Created(Order order) {
            super(order);
        }
    }

    public static class Paid extends State {

        protected Paid(Order order) {
            super(order);
        }
    }

    // Events


    abstract static class Event implements CborSerializable {

    }

    public static final class OnCreated extends Event {
        public final ActorRef<Order> ref;

        public OnCreated(ActorRef<Order> ref) {
            this.ref = ref;
        }
    }

    // Comands

    interface Command extends CborSerializable {
    }

    public static final class CreateOrder implements Command {
        final String orderId;
        //final int quantity;
        final ActorRef<OrderResponse> replyTo;

        public CreateOrder(String orderId, ActorRef<OrderResponse> replyTo){
            this.orderId = orderId;
            this.replyTo = replyTo;
        }
    }

    public final static class OrderResponse implements Command {
        public final Integer orderId ;
        public OrderResponse(Integer orderId) {
            this.orderId = orderId;
        }
    }

    public static final class PaymentDone implements Command {
        final String orderId;
        final ActorRef<OrderResponse> replyTo;

        public PaymentDone(String orderId, ActorRef<OrderResponse> replyTo){
            this.orderId = orderId;
            this.replyTo = replyTo;
        }
    }



    @Override
    public State emptyState() {
        return null;
    }

    @Override
    public CommandHandlerWithReply commandHandler() {
        CommandHandlerWithReplyBuilder<Command, Event, State> builder = newCommandHandlerWithReplyBuilder();

        builder.forNullState().onCommand(CreateOrder.class, this::createOrder);
        //builder.forStateType(Open.class).onCommand(CreateOrder.class, this::createOrder);
        return builder.build();
    }

    private ReplyEffect<Event, State> createOrder(State state, Command command) {
        // TODO implement

//            return Effect()
//                .persist(new Created(new Order()))
//                .thenReply(cmd.replyTo, updatedCart -> StatusReply.success(updatedCart.toSummary()));
            return Effect().noReply();
    }


    @Override
    public EventHandler eventHandler() {
        // TODO implement
        return null;
    }





    private final String orderId;

    private Order(String orderId) {
        super(
                PersistenceId.of(ENTITY_KEY.name(), orderId),
                SupervisorStrategy.restartWithBackoff(Duration.ofMillis(200), Duration.ofSeconds(5), 0.1));
        this.orderId = orderId;
        // this.projectionTag = projectionTag;
    }



    public static Behavior<Command> create(String orderId) { //, String projectionTag) {
        return Behaviors.setup(
                ctx -> EventSourcedBehavior.start(new Order(orderId), ctx));
    }

    static final EntityTypeKey<Command> ENTITY_KEY =
            EntityTypeKey.create(Command.class, "Order");


    static final List<String> TAGS =
            Collections.unmodifiableList(
                    Arrays.asList("carts-0", "carts-1", "carts-2", "carts-3", "carts-4"));

    public static void init(ActorSystem<?> system) {
        ClusterSharding.get(system)
                .init(
                        Entity.of(
                                ENTITY_KEY,
                                entityContext -> {
                                    int i = Math.abs(entityContext.getEntityId().hashCode() % TAGS.size());
                                    //String selectedTag = TAGS.get(i);
                                    return Order.create(entityContext.getEntityId());//, selectedTag);
                                }));
    }

}
