package kapsky.ordermanagement;

import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.management.cluster.bootstrap.ClusterBootstrap;
import akka.management.javadsl.AkkaManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {
        ActorSystem<Void> system = ActorSystem.create(Behaviors.empty(), "OrderManagementService");
        try {
            init(system);
        } catch (Exception e) {
            logger.error("Terminating due to initialization failure.", e);
            system.terminate();
        }
    }

    public static void init(ActorSystem<Void> system) {

        //AkkaManagement.get(system).start();
        //ClusterBootstrap.get(system).start();

        // TODO my persisent actor init
        Order.init(system);
        // ShoppingCart.init(system);

//        ApplicationContext springContext =
//                SpringIntegration.applicationContext(system.settings().config());

//        ItemPopularityRepository itemPopularityRepository =
//                springContext.getBean(ItemPopularityRepository.class);
        // JpaTransactionManager transactionManager = springContext.getBean(JpaTransactionManager.class);

        //ShoppingCartService grpcService = new ShoppingCartServiceImpl(system, itemPopularityRepository);

        OrderServiceImpl orderService = new OrderServiceImpl(system);


        OrderRoutes orderRoutes = new OrderRoutes(orderService);//context.getSystem(), userRegistryActor);
        OrderServer.startHttpServer(orderRoutes.createRoutes(), system);

    }


}
