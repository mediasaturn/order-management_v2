package kapsky.ordermanagement;

import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.Route;

import static akka.http.javadsl.server.Directives.*;

public class OrderRoutes {
    private final OrderServiceImpl orderService;

    public OrderRoutes(OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    public Route createRoutes() {
        return pathPrefix("createOrder", () ->
                concat(
                        //#users-get-delete
                        pathEnd(() ->
                                concat(

                                        post(() ->
                                                entity(
                                                        Jackson.unmarshaller(OrderServiceImpl.OrderDto.class),
                                                        orderDto ->
                                                                onSuccess(orderService.createOrder(orderDto), performed -> {
                                                                    // log.info("Create result: {}", performed.description);
                                                                    return complete(StatusCodes.CREATED, performed, Jackson.marshaller());
                                                                })
                                                )
                                        )
                                )
                        )// ,
                )
        );
    }
    //#all-routes
}
