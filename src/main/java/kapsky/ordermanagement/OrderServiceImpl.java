package kapsky.ordermanagement;

import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Scheduler;
import akka.actor.typed.javadsl.AskPattern;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.CompletionStage;

public class OrderServiceImpl {
    private final static Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);
    //private final ActorRef<UserRegistry.Command> orderActor;
    private final Duration askTimeout;
    private final Scheduler scheduler;
    private final ClusterSharding sharding;

    public OrderServiceImpl(ActorSystem<?> system ){//, ActorRef<UserRegistry.Command> orderActor) {
        //this.orderActor = orderActor;
        scheduler = system.scheduler();
        askTimeout = system.settings().config().getDuration("my-app.routes.ask-timeout");
        sharding = ClusterSharding.get(system);
    }

    public final static class OrderDto {
        //public final List<String> items;
        public final String itemId;
        @JsonCreator
        public OrderDto(@JsonProperty("itemId") String itemId) {
            this.itemId = itemId;
        }
    }

    public CompletionStage<Order.OrderResponse> createOrder(OrderDto in) {
        //logger.info("addItem {} to cart {}", in.getItemId(), in.getCartId());
        EntityRef<Order.Command> entityRef =
                sharding.entityRefFor(Order.ENTITY_KEY, in.itemId);
        // TODO ask with status and scheduler
        return entityRef.ask(replyTo -> new Order.CreateOrder(in.itemId, replyTo), askTimeout);//, scheduler);

    }
}
